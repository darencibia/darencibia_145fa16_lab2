import java.util.Scanner;

/**
 * Application class used to test the capabilities of class Person
 *
 * @author dianaa@email.sc.edu
 * @version 2016.11.28
 */
public class PersonTest
{
	public static void main( String[] args )
	{
		// instantiate Scanner object for reading in keyboard input
		Scanner input = new Scanner( System.in );

		// prompt user to input name
		System.out.println("Enter the first person's name: ");

		// read in the name
		String name1 = input.nextLine();

		// prompt user to input SSN
		System.out.println(
				"Enter the first person's 9-digit SSN (no spaces or dashes): ");

		// read in the SSN
		int ssn1 = input.nextInt();

		// prompt user to input voter registration status as true or false
		System.out.println(
				"Enter the first persons's voter registration status (true or false): ");
		
		// read in the voter status 
		boolean voterStatus1 = input.nextBoolean();

		// prompt user to input annual salary
		System.out.println(
				"Enter the first person's annual salaray as a decimal value (no dollar sign or commas): ");

		// read in the salary amount
		double salary1 = input.nextDouble();

		// Create new Person object using the parameters above, and assign 
		// object reference to newly declared person1 object variable
		Person person1 = new Person(name1, ssn1, voterStatus1, salary1 );

		// Create new Person object using the parameters given in the
		// homework instructions, and assign object reference to person2
		Person person2 = new Person( "Barry Obama", 987654321,  false, -123.45);

		// Print a blank line
		System.out.println();

		// Display person1's information
		person1.displayInfo();
		
		// Print a blank line
		System.out.println();

		// Display person2's information
		person2.displayInfo();

		// update person2's information by calling each of the
		// four instance methods
		person2.setName( "Barack Hussein Obama" );
		person2.setSocialSecurityNumber( 123456789 );
		person2.setVoterRegistrationStatus( true );
		person2.setAnnualSalary( 400000.00 );

		// Print a blank line
		System.out.println();
				
		// Print a message indicating that person2's info has been changed
		System.out.println( "Person 2's information has been changed to: " );

		// Print a blank line
		System.out.println();
				
		// Display person2's updated information 
		person2.displayInfo();
		
	} // end method main

} // end class PersonTest
