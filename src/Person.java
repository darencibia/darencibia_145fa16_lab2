/**
 * Class used to define Person objects, which have the following attributes:
 * name, social security number, voter registration status, and annual salary
 *
 * @author dianaa@email.sc.edu
 * @version 2016.11.28
 */
public class Person
{
	/*
	 * FIELD(S)
	 */
	private String name;
	private int socialSecurityNumber;
	private boolean voterRegistrationStatus;
	private double annualSalary;

	/*
     * CONSTRUCTOR(S)
     */
	/**
	 * @param name
	 * @param socialSecurityNumber
	 * @param voterRegistrationStatus
	 * @param annualSalary
	 */
	public Person(String name, 
				  int socialSecurityNumber, 
				  boolean voterRegistrationStatus, 
				  double annualSalary)
	{
		this.name = name;
		this.socialSecurityNumber = socialSecurityNumber;
		this.voterRegistrationStatus = voterRegistrationStatus;
		
		// check to see if supplied annual salary is valid;
		// if not, the annualSalary instance variable should remain
		// at its default value of 0.0
		if ( annualSalary >= 0.0 )
		{
			this.annualSalary = annualSalary;
		} //end if
	} // end 4 arg constructor for Person class

    /*
     * METHOD(S)
     */
	/**
	 * @return the name
	 */
	public String getName() 
	{
		return name;
	} //end getName

	/**
	 * @param name the name to set
	 */
	public void setName(String name) 
	{
		this.name = name;
	} //end setName

	/**
	 * @return the socialSecurityNumber
	 */
	public int getSocialSecurityNumber() 
	{
		return socialSecurityNumber;
	} //end getSocialSecurityNumber

	/**
	 * @param socialSecurityNumber the socialSecurityNumber to set
	 */
	public void setSocialSecurityNumber(int socialSecurityNumber) 
	{
		this.socialSecurityNumber = socialSecurityNumber;
	} //end setSocialSecurityNumber

	/**
	 * @return the voterRegistrationStatus
	 */
	public boolean getVoterRegistrationStatus() 
	{
		return voterRegistrationStatus;
	} //end getVoterRegistrationStatus

	/**
	 * @param voterRegistrationStatus the voterRegistrationStatus to set
	 */
	public void setVoterRegistrationStatus(boolean voterRegistrationStatus) 
	{
		this.voterRegistrationStatus = voterRegistrationStatus;
	} //end setVoterRegistrationStatus

	/**
	 * @return the annualSalary
	 */
	public double getAnnualSalary() 
	{
		return annualSalary;
	} //end getAnnualSalary

	/**
	 * @param annualSalary the annualSalary to set
	 */
	public void setAnnualSalary(double annualSalary) 
	{
		this.annualSalary = annualSalary;
	} //end setAnnualSalary
 
	/**
     * Displays the current Person object's information 
     * on 4 separate lines
     */
    public void displayInfo()
    {
    	System.out.printf(
    			"Name: %s\nSSN: %d\nVoter Status: %b\nSalary: $%.2f\n",
    			getName(),
    			getSocialSecurityNumber(),
    			getVoterRegistrationStatus(),
    			getAnnualSalary()
    	);
    } // end method displayInfo

} // end class Person

